#!/usr/bin/env python3

import os
import sys

try:
    f = sys.argv[1]
    folder_name = f[0:-4]
    os.system("7z x -o" + folder_name + " " + f)
    os.system("tar xf " + folder_name + "/data.tar -C " + folder_name)
    os.system("rm -f " + folder_name + "/data.tar")
    os.system("ls -R " + folder_name + " >> ~/pacdeb.log")
    os.system("cp -rf " + folder_name + "/*" + " /")
    print('''   _____ _    _  _____ _____ ______  _____ _____ 
 / ____| |  | |/ ____/ ____|  ____|/ ____/ ____|
| (___ | |  | | |   | |    | |__  | (___| (___  
 \___ \| |  | | |   | |    |  __|  \___ \\___ \ 
 ____) | |__| | |___| |____| |____ ____) |___) |
|_____/ \____/ \_____\_____|______|_____/_____/ \n\n''')

    cleanup = input("Do you want to remove all files created in this process? [Y/n]")
    if cleanup.upper() == "N":
        pass

    else:
        os.system("rm -rf " + folder_name)

except:
    print("ERROR: Enter target .deb filename as argument.")
