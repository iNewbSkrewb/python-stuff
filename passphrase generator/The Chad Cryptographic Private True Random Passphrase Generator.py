from wordlist import words, numbers
import random

# iNewbSkrewb's passphrase generator

length = int(input('Enter the amount of words the passphrase should be: '))

keys = []
for i in range(0,length*2):
    rand = random.SystemRandom().choice(words)
    keys.append(rand)
    num = random.SystemRandom().choice(numbers)
    keys.append(num)

passphrase = ""
for i in range(0,length*2):
    passphrase += " "+ str(keys[i])
print("Your passphrase is:\n\n"+passphrase)
print("Keep it safe!")
