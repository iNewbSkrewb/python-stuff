from wordlist import words
from numsym import numsyms
import requests

length = input('Enter the amount of words the passphrase should be: ')

parameters = {
    "jsonrpc": "2.0",
    "method": "generateIntegers",
    "params": {
        "apiKey": "4e8cb85a-b0d0-4cfb-85fb-e3b557b32944",
        "n": length,
        "min": 0,
        "max": 7775,
        "replacement": False
    },
    "id": 22769
}
response = requests.post('https://api.random.org/json-rpc/2/invoke', json=parameters)

keys = []
for i in response.json()['result']['random']['data']:
    keys.append(i)

passphrase = ""
for i in keys:
    passphrase += " "+ words[i]
print("Your passphrase is:\n\n" + passphrase)
print("\nBe sure to keep it somewhere safe and secure!")
