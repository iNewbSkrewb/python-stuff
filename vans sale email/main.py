import requests, smtplib, ssl
from bs4 import BeautifulSoup
from config import *


context = ssl.create_default_context()
def send_email(msg):
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls(context=context)
        server.login(email, passwd)

        server.sendmail(email, recipient, msg)
    except Exception as e:
        print(e)
    finally:
        server.quit()


r = requests.get(link) # gets data from page
s = BeautifulSoup(r.text, 'html.parser')

current_price = float(s.find("meta", property="og:price:amount")["content"]) # gets current price
product_name  = s.title.text.split(" |")[0] # gets the name of the product for the email

email_msg = f'''\
Subject: The stuff you wanted from Vans is on sale!!

The {product_name} is on sale for ${current_price:.2f}.

WAS: ${original_price}
'''

if current_price < original_price:
    send_email(email_msg) # sends the email using the information in config.py
