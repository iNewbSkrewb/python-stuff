# Samien's Vans shoe sale notification program

This is just something I quickly threw together to find out when a specific pair of shoes I want goes on sale (you can actually see which pair in config.py).

To use this for yourself, all you have to do is follow the instructions below.
## Usage

First and foremost, you're going to need to already have at least Python 3 installed. There are plenty of resources online to help you get started with that, such as [this one](https://lmddgtfy.net/?q=How%20to%20install%20python%20on%20windows) one for Windows, or [this one](https://lmddgtfy.net/?q=how%20to%20install%20python%20on%20macos) for macOS. If you're on Linux, you probably know what you're doing.

"Installation" and usage is quite simple, just do the following:

- Download [main.py](https://gitlab.com/iNewbSkrewb/python-stuff/-/raw/main/vans%20sale%20email/main.py?inline=false)

- Download [config.py](https://gitlab.com/iNewbSkrewb/python-stuff/-/raw/main/vans%20sale%20email/config.py?inline=false)

*For the next three points, make your edits in quotes after the equal sign, like how the pre-existing example is.*

- Edit *config.py* using any text editor of your choice and change the `link` variable to the link of the pair of shoes you're waiting for to go on sale.

- Do the same with the `original_price` variable, this time making it the original price of the shoes you are looking at.

- Lastly, edit the `recipient` field and set it to your email.

And now you're done!!  
...  
Not quite. Now you have to do some very hard work and find out how to get a script running in the background at certain intervals. On Linux, I use a cronjob, but I don't really know for anything else. Windows has a built in tool for auto-running scripts, and I think macOS also might have one, but that's up to you to find.

Once you get this running on an interval, make sure you don't separate or move the files.  
Besides that, you should be good to go. Now you'll recieve an email when the shoes you want go on sale!


*Note: I don't really care if strangers use the email I use for the email sending, but if you want to use a sender email of your own, you have to make an email which is set up for being accessed from external applications. If it's not gmail, you'll need to change a few more things in `main.py`*
