import paramiko
import sys

ssh = paramiko.SSHClient()
file_name = sys.argv[1]
lab_pem = sys.argv[2]
username = "hpecloud"
port = "22"
def write(host):
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    key = paramiko.RSAKey.from_private_key_file(lab_pem)
    ssh.connect(host, port, username,pkey=key)
    ssh.get_transport()

    command = "sudo su -c '/usr/localcw/opt/sact/SACT_collect.ksh -u'"

    stdin, stdout, stderr = ssh.exec_command(command=command,get_pty=True)
    print("Original command sent!")
    stdin.flush()
    print("Input flushed!")

    if stderr.channel.recv_exit_status() != 0:
       print("Error occured!")
       print(f"The following error occured: {stderr.readlines()}")
    else:
       print(f"Getting output {host}")
       print(f"The following output was produced: \n{stdout.readlines()}")
    ssh.close()

hostnames = open(file_name, "r").readlines()
for host in hostnames:
    host = host.strip()
    host = host.split("\n")
    for i in host:
        try:
            write(i)
        except:
            print(f"WARNING: {i} is failed")
